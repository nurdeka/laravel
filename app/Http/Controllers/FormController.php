<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form()//
    {
        return view('data.form');
    }

    public function post(Request $request)
    {
        // dd($request->all);
        $fnama = $request->nama_depan;
        $lnama = $request->nama_belakang;
        $alamat =$request->alamat;

        return view('data.index', compact('fnama','lnama', 'alamat'));
    }
}
