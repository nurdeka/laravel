<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Data Form Registrasi</h1>
    <form action='/post' method='POST'>
        @csrf
        <label for='fnama'>First Name</label><br>
        <input type='text' name='nama_depan' id='fnama'><br><br>
        <label for='lnama'>Last Name</label><br>
        <input type='text' name='nama_belakang' id='lnama'><br><br>
        <label>Gender :</label><br>
        <input type="radio" name="gender" value="0" >Male<br>
        <input type="radio" name="gender" value="1" >Female<br>
        <input type="radio" name="gender" value="2" >Other<br>
        <br>
        <label>Nationality :</label><br>
        <select>
            <option>Indonesian</option>
            <option>Malaysian</option>
            <option>Singaporean</option>
            <option>Australian</option>
            <option>Other</option>
        </select>
        <br>
        <br><label>Language Spoken :</label><br>
        <input type="checkbox" name="laguage" value="0" >Bahasa Indonesia<br>
        <input type="checkbox" name="laguage" value="1" >English<br>
        <input type="checkbox" name="laguage" value="2" >Mandarin<br>
        <input type="checkbox" name="laguage" value="3" >Other<br>
        <br>
        <label for='address'>Address</label><br>
        <textarea name='alamat' id='address' cols='30' rows='10'></textarea><br>
        <input type='submit' value='Kirim'>
    </form>
</body>
</html>